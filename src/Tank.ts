import { mapHeight, mapWidth } from "./config";

export class Tank {
  x: number;
  y: number;
  gas: number;

  constructor(x: number, y: number, startingGas: number) {
    this.x = x;
    this.y = y;
    this.gas = startingGas;
  }

  private consumeGas() {
    this.gas = this.gas - 1;
  }

  getCoordinates() {
    return { x: this.x, y: this.y };
  }

  getGas() {
    return this.gas;
  }

  moveUp() {
    const newCoordinate = this.y - 1;
    if (newCoordinate >= 0) {
      this.y = newCoordinate;
      this.consumeGas();
    }
  }

  moveLeft() {
    const newCoordinate = this.x - 1;

    if (newCoordinate >= 0) {
      this.x = newCoordinate;
      this.consumeGas();
    }
  }

  moveDown() {
    const newCoordinate = this.y + 1;
    if (newCoordinate < mapHeight) {
      this.y = newCoordinate;
      this.consumeGas();
    }
  }

  moveRight() {
    const newCoordinate = this.x + 1;

    if (newCoordinate < mapWidth) {
      this.x = newCoordinate;
      this.consumeGas();
    }
  }
}
