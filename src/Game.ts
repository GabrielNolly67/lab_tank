import readline from "readline";
import { mapHeight, mapWidth, tankModel } from "./config";
import { Tank } from "./Tank";

export class Game {
  tank: Tank;
  gameMap: string[][] = [];

  constructor() {
    this.tank = new Tank(0, 0, 32);
  }

  start() {
    this.gameLoop();

    // create readline interface to collect every keypress
    readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    const stdin = process.openStdin();

    // read user input and move the tank
    stdin.addListener("data", (data) => {
      const keycode: string = data.toString();
      this.handleKeypress(keycode);
    });
  }

  private handleKeypress(key: string) {
    const gas = this.tank.getGas();

    if (gas <= 0) {
      this.gameLoop();
      return;
    }

    switch (key) {
      case "w":
        this.tank.moveUp();
        break;
      case "a":
        this.tank.moveLeft();
        break;
      case "s":
        this.tank.moveDown();
        break;
      case "d":
        this.tank.moveRight();
        break;

      default:
        break;
    }

    this.gameLoop();
  }

  private printGameMap() {
    console.clear();
    console.log("=".repeat(mapWidth + 2));
    this.gameMap.forEach((row) => {
      console.log("|" + row.join("") + "|");
    });
    console.log("=".repeat(mapWidth + 2));
  }

  // render the game map, tank and instructions
  private gameLoop() {
    const gas = this.tank.getGas();
    const tankCoordinates = this.tank.getCoordinates();

    const hasGas = gas > 0;

    for (let heightIndex = 0; heightIndex < mapHeight; heightIndex++) {
      this.gameMap[heightIndex] = ".".repeat(mapWidth).split("");
    }

    this.gameMap[tankCoordinates.y][tankCoordinates.x] = tankModel;

    this.printGameMap();

    if (hasGas) {
      console.log("[w,a,s,d]");
      console.log(`GAS: ${gas}`);
    } else {
      console.log("NO GAS!");
    }
  }
}
